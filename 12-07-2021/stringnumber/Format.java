package com.divergentsl.corejava.stringnumber;

import java.text.NumberFormat;
import java.util.Locale;

public class Format {
	public static void main(String[] args)
	{
		int n=100000;
		NumberFormat form1=NumberFormat.getCurrencyInstance(Locale.US);
		NumberFormat form2=NumberFormat.getCurrencyInstance(Locale.ITALIAN);
		String curr1=form1.format(n);
		String curr2=form2.format(n);
		System.out.println("for us:"+curr1);
		System.out.println("for italian:"+curr2);
		
	}

}
