package com.divergentsl.corejava.stringnumber;

public class BooleanLogic {
	public static void AND()
	{
	    int a[] = { 1, 0, 1, 0, 1 };
	    int b[] = { 0, 1, 1, 0, 0 };
	    int i, ans;
	  
	    for (i = 0; i < 5; i++) {
	        if (a[i] == 0 && b[i] == 0)
	            ans = 0;
	  
	        else if (a[i] == 0 && b[i] == 1)
	            ans = 0;
	  
	        else if (a[i] == 1 && b[i] == 0)
	            ans = 0;
	        else
	            ans = 1;
	    
	        System.out.println( ""+a[i] +","+b[i]+"," +ans);
	    }
	}
	public static void OR()
	{
		 int a[] = { 1, 0, 1, 0, 1 };
		    int b[] = { 0, 1, 1, 0, 0 };
		    int i, orans;
		  
		    for (i = 0; i < 5; i++) {
		  
		        // using the if-else conditions
		        if (a[i] == 0 && b[i] == 0)
		            orans = 0;
		        else
		            orans = 1;
		  
		        System.out.println(""+a[i]+"," +b[i]+","+orans);
		    }
	}
	public static void main(String[] args) {
		
		BooleanLogic.AND();
		BooleanLogic.OR();
		
	}
}


	


