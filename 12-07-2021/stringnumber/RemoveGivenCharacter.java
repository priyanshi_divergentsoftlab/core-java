package com.divergentsl.corejava.stringnumber;

import java.util.Scanner;

public class RemoveGivenCharacter {

	public static void main(String[] args) {  
        String str = "Priyanshi sahu";  
        
       //Scanner n = new Scanner(System.in);
        System.out.println("Enter a string:");
	    //  str = n.nextLine();
        System.out.println(RemoveGivenCharacter.charRemoveAt(str,2));  
     }  
     public static String charRemoveAt(String str, int n) {  
        return str.substring(0, n) + str.substring(n+1);  
     }  
}
