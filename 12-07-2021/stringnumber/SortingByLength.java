package com.divergentsl.corejava.stringnumber;

public class SortingByLength {
	
	static void sorting(String[]s, int n)
	{
		for (int i=1; i<n ; i++)
		{
			String temp = s[i];
			int j=i-1;
			 while (j >= 0 && temp.length() < s[j].length())
		        {
		            s[j+1] = s[j];
		            j--;
		        }
		        s[j+1] = temp;
		    }
		}
		
		static void printArraystring(String str[], int n)
		{
		    for (int i=0; i<n; i++)
		        System.out.print(str[i]+" ");
		}
		 
		
		public static void main(String args[])
		{
		    String []arr = {"Divergent", "Software", "labs", "pvtltd"};
		    int n = arr.length;
		    
		    sorting(arr,n);
		    
		    printArraystring(arr, n);
		     
		}
			
}


