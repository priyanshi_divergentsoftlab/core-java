package com.divergentsl.corejava.stringnumber;

import java.util.Arrays;
import java.util.List;

public class JoinByDelimiter {

	public static void main(String[] args)
	{
		
	List<String> alphabets = Arrays. asList("D", "I", "V", "E", "R", "G", "E", "N", "T");
	String delimiter = ",";
	String result = String. join(delimiter, alphabets);
	

    System.out.println(result);
}
	}
