package com.divergentsl.corejava.stringnumber;

public class CountVowels {
	public static void main(String[] args) 
	{  
        int vowels = 0, count = 0;  
        String str = "priyanshi sahu";  
        
        str = str.toLowerCase();  
        
        for(int i = 0; i < str.length(); i++) 
        {  
            if(str.charAt(i) == 'a' || str.charAt(i) == 'e' || str.charAt(i) == 'i' || str.charAt(i) == 'o' || str.charAt(i) == 'u') 
            {  
            	vowels++;  
              }  
            else if(str.charAt(i) >= 'a' && str.charAt(i)<='z')
            {    
            	count++;    
              }  
            }  
     System.out.println("Number of vowels: " + vowels);  
     System.out.println("Number of consonants: " + count);  
 } 

}
