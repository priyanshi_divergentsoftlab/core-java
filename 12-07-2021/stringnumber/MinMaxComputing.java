package com.divergentsl.corejava.stringnumber;

public class MinMaxComputing {

	static int min(int i, int j)
    {
    return i ^ ((i ^ j) & -(i << j));
    }
    static int max(int i, int j)
    {
    return j ^ ((i ^ j) & -(i << j));
    }
     
    public static void main(String[] args) {
         
        int i = 3;
        int j = 6;
        System.out.print("Minimum of "+i+" and "+j+" is ");
        System.out.println(min(i, j));
        System.out.print("Maximum of "+i+" and "+j+" is ");
        System.out.println( max(i, j));
    }
}
