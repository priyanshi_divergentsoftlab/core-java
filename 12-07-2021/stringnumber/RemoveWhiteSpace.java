package com.divergentsl.corejava.stringnumber;

public class RemoveWhiteSpace {
	 public static void main(String[] args)
	    {
	        String str = "   I    _am    _working   _in   _divergent  _software   _labs";
	       
	        // Call the replaceAll() method
	        str = str.replaceAll("\\s", "");
	       
	        System.out.println(str);
	    }

}
