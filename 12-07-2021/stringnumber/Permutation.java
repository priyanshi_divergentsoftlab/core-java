package com.divergentsl.corejava.stringnumber;

import java.util.Scanner;

public class Permutation {
	
	static void combination(String c, String pattern)
	{
		if (c.length()== 0)
		{
			System.out.println(pattern + "  ");
			return;
		}
		for(int i=0; i<c.length(); i++)
		{
			char name = c.charAt(i);
			String left_str = c.substring(0,i);
			String right_str = c.substring(i+1);
			String rest = left_str + right_str;
			combination(rest,pattern +name);
			
		}
	}
	public static void main(String[] args)
	{
		Scanner scan = new Scanner(System.in);
		
		String str;
		String pattern=" ";
		
		System.out.println("Enter a string:");
		str = scan.next();
		
		System.out.println("combinations could be:");
		combination(str,pattern);
		
	}

}
