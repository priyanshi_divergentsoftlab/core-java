package com.divergentsl.corejava.stringnumber;

public class FloatingPointValue {
	 public static void main(String[] args) {
	        float fn = 0.9f;
	        
	        System.out.println("\nInitial floating number: " + fn);
	        
	        float nextdownfn = Math.nextDown(fn);
	        
	        float nextupfn = Math.nextUp(fn);
	        
	        System.out.println("Float " + fn + " next down is " + nextdownfn);
	        
	        System.out.println("Float " + fn + " next up is " + nextupfn);
	        
	        double dn = 0.8d;
	       
	        System.out.println("\nInitial double number: " + dn);		
	        
	        double nextdowndn = Math.nextDown(dn);
	        
	        double nextupdn = Math.nextUp(dn);
	        
	        System.out.println("Double " + dn + " next down is " + nextdowndn);
	        
	        System.out.println("Double " + dn + " next up is " + nextupdn);
	    }
	 

}
