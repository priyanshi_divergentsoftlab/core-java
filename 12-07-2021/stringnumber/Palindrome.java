package com.divergentsl.corejava.stringnumber;

import java.util.Scanner;

public class Palindrome {
	
	 public static void main(String args[])
	   {
	      String str, len = "";
	      Scanner sc = new Scanner(System.in);
	 
	      System.out.println("Enter a string:");
	      str = sc.nextLine();
	 
	      int length = str.length();
	 
	      for ( int i = length - 1; i >= 0; i-- )
	         len = len + str.charAt(i);
	 
	      if (str.equals(len))
	      {
	         System.out.println(str+" is a palindrome");
	      }
	      else {
	    	  
	      
	         System.out.println(str+" is not a palindrome");
	      }
	   }

	
}