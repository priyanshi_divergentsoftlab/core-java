package com.divergentsl.corejava.stringnumber;

import java.util.Arrays;

public class LongestPrefix {

	
	static String commonPrefix(String[] arr) 
	{
        int size= arr.length;
         
        if(size==0)
        	return"";
        Arrays.sort(arr);
        int end =Math.min(arr[0].length(), arr[size-1].length());
        int i = 0;
        while(i<end&&arr[0].charAt(i)== arr[size-1].charAt(i)){
        	i++;
        	}
        String pre = arr[0].substring(0,i);
        return pre;
        
        }
    public static void main(String[] args) 
    {
        LongestPrefix p =new LongestPrefix();
        String[] input = {"shoot","shot","shut","shootout"};
        System.out.println("the longest common prefix is:"+p.commonPrefix(input));
    }
	

}
