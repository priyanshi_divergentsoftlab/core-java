package com.divergentsl.corejava.stringnumber;

public class RemoveLeadingSpaces {
	public static void main(String[] args) {
		
	      String str = " priyanshi sahu ";
	      
	      System.out.println("Text = "+str);
	      str = str.replaceAll("\\s", "");
	      
	      System.out.println("Text after using trim() method =" + str.trim());
	    }

}
