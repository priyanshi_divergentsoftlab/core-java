package com.divergentsl.corejava.stringnumber;

public class NonRepeatingCharacter {
	
   public static void main(String[] args) {
	   
	   String name ="PRIYANSHI";
	   
	   System.out.println("the string is:"+name);
	   
	   for(int i =0; i<name.length(); i++) {
		   boolean unique =true;
		   
		   for(int j=0; j<name.length(); j++) {
			   
			   if(i!=j && name.charAt(i) == name.charAt(j)) {
				   unique = false;
				   break;
			   }
		   }
		   if (unique) {
			   
			   System.out.println("the first non repeating character is:"+name.charAt(i));
			   
			   break;
		   
		   }
	   }
}
	
}
