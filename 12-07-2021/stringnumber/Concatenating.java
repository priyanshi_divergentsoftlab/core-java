package com.divergentsl.corejava.stringnumber;

public class Concatenating {

	static String repeat(String s, int n)
    {
         
        String s1 = s;
     
        for (int i = 1; i < n; i++)
        
            s += s1;
     
        return s;
    }
     
    public static void main(String[] args)
    {
        String s = "divergent";
        int n = 2;
        System.out.println(repeat(s, n));
    }
}
