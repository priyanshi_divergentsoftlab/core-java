package com.divergentsl.corejava.stringnumber;

import java.math.BigInteger;

public class ExtractPrimitiveFromBiginteger {
	public static void main(String[] args)
	{
		
        BigInteger bigval = BigInteger.valueOf(Long.MAX_VALUE);
        System.out.println("\nBigInteger value: "+bigval);
        
		long valLong = bigval.longValue();
		System.out.println("\nConvert the said BigInteger to an long value: "+valLong);
		
        int valInt = bigval.intValue();
		System.out.println("\nConvert the said BigInteger to an int value: "+valInt);
		
        short valShort = bigval.shortValue();
        System.out.println("\nConvert the said BigInteger to an short value: "+valShort);
        
        byte valByte = bigval.byteValue();
        System.out.println("\nConvert the said BigInteger to byte: "+valByte);
        
        long valExactLong = bigval.longValueExact();
		System.out.println("\nConvert the said BigInteger to exact long: "+valExactLong);
    }
	

}
