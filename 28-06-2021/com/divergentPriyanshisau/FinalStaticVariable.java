package com.divergentPriyanshisau;

public class FinalStaticVariable {
	public static void main(String[] args) 
    {
        // a final reference variable sb
        final StringBuilder sb = new StringBuilder("Divergent");
          
        System.out.println(sb);
          
        // changing internal state of object
        // reference by final reference variable sb
        sb.append("Divergent");
          
        System.out.println(sb);
    }    


}
