package com.divergentPriyanshisau;

public class ConstructorChaining {
	
	    // block to be excuted first
	    {
	        System.out.println("first");
	    }
	    ConstructorChaining()
	    {
	        System.out.println("default");
	    }
	    ConstructorChaining(int x)
	    {
	        System.out.println(x);
	    }
	  
	    // block to be executed after the first block
	    // which has been defined above.
	    {
	        System.out.println("second");
	    }
	    public static void main(String args[])
	    {
	        new ConstructorChaining();
	        new ConstructorChaining(10);
	    }
	}


