package com.divergentPriyanshisau;

public class NullPointer {
	
	public static void main(String[] args) {  
		
		String str = null;  
		String msg = (str == null) ? "null value" : str.concat("JTP");  
		System.out.println(msg);  
		  
		str = "Null Exception ";  
		msg = (str == null) ? "null value" : str.concat("Avoided");  
		System.out.println(msg);  
		
	}  
}
