package com.divergentPriyanshisau;

public class MethodOverloading {
	  
		  void run()
		  {
			  System.out.println("Method Overloading is running");
		  }  
		}  
		//Creating a child class  
		class overload extends MethodOverloading{  
		  public static void main(String args[]){  
		  //creating an instance of child class  
		  overload obj = new overload();  
		  //calling the method with child class instance  
		  obj.run();  
		  }  
		}  


